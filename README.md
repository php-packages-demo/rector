# rector

[rector/rector](https://packagist.org/packages/rector/rector) Automate refactoring

* [*Rector - The Power of Automated Refactoring Book Released*
  ](https://tomasvotruba.com/blog/rector-the-power-of-automated-refactoring-book-released/)
  2021-05 (Matthias Noback and) Tomáš Votruba
* [*Early release of Rector - The power of automated refactoring*
  ](https://matthiasnoback.nl/2021/05/early-release-of-the-rector-book/)
  2021-05 Matthias Noback (and Tomáš Votruba)
* [*Rector: Part 1 - What and How*
  ](https://tomasvotruba.com/blog/2018/02/19/rector-part-1-what-and-how/)
  2018-2020 Tomáš Votruba

